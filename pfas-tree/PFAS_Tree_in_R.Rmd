---
title: "Interacting with the PubChem PFAS Tree in R"
author: 
- "Emma L. Schymanski^1^*"
date: "19 March 2023"
output: pdf_document
csl: journal-of-cheminformatics.csl
bibliography: refs.bib
urlcolor: blue
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_chunk$set(warning = FALSE, message = FALSE) 
#knitr::opts_chunk$set(fig.pos = "!H", out.extra = "")
```

^1^ Luxembourg Centre for Systems Biomedicine (LCSB), 
University of Luxembourg, 6 avenue du Swing, 4367, Belvaux, Luxembourg. 
*ELS: [emma.schymanski@uni.lu](mailto:emma.schymanski@uni.lu).
ORCID: ELS: [0000-0001-6868-8145](http://orcid.org/0000-0001-6868-8145),

## Preamble

This is an addendum to the document describing the 
"[PFAS and Fluorinated Compounds in PubChem Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)" 
(hereafter 
"[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)"), specifically describing how to interact with the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120) in R. 

The full documentation of the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)
can be found 
[here](https://gitlab.lcsb.uni.lu/eci/pubchem-docs/-/blob/pfas-docs-updates/pfas-tree/PFAS_Tree.pdf).


## Interacting with the PubChem PFAS Tree in R

The following contains a few tips to start interacting with the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120) in R;
note that some of these features are also in active development. 


To start, set up all packages, download latest scripts and source them: 
```{r initialize} 
library(rjson)
library(httr)
source("getPcHidTree.R")
```

Then, retrieve the information about how the PFAS tree is constructed. 
The hid number
is from the URL, _i.e._ hid=120. The following code retrieves the 
details and structure to a depth of 3 (further subnodes can be 
retrieved by increasing the number). 

```{r get PFAS tree}
tree_csv <- getPcHidTree(120,3)
tree_csv
```

Then, load the file and take a look:
```{r read PFAS tree}
PFAS_tree <- read.csv(tree_csv, stringsAsFactors = F)
PFAS_tree[1:7,c(6,7,10)]
```

It is possible to subset by keyword to retain only an interesting subset of 
entries, _e.g._, here to find the OntoChem lists out of the PFAS collections:  
```{r find OntoChem lists}
i_OntoChem_lists <- grep("OntoChem",PFAS_tree$nodeNames)
OntoChem_PFAS_lists <- PFAS_tree[i_OntoChem_lists,]
OntoChem_PFAS_lists[,c(6,7,10)]
```

Once the PFAS Tree output is available, including node HNIDs, it is 
possible to build the URL needed to retrieve the CID listings 
per node entry via 
[PUG REST](https://pubchemdocs.ncbi.nlm.nih.gov/pug-rest$classification_nodes).

```{r add URL}
# https://pubchem.ncbi.nlm.nih.gov/rest/pug/classification/hnid/<integer>/<id type>/<format>
hnid_base_url <- "https://pubchem.ncbi.nlm.nih.gov/rest/pug/classification/hnid/"
hnid_end_url <- "/cids/TXT"
PFAS_tree$REST_URL <- ""

for (i in 1:length(PFAS_tree$nodeHNID)) {
  PFAS_tree$REST_URL[i] <- paste0(hnid_base_url,PFAS_tree$nodeHNID[i],hnid_end_url)
}
```

Finally, write the output for further use: 

```{r output list} 
write.csv(PFAS_tree, file="PubChem_PFAS_Tree_Details.csv",row.names = F)
```


## Contact Details

For questions about the specific content of this document, 
please reach out to [Emma Schymanski](mailto:emma.schymanski@uni.lu).

For general questions about PubChem, please reach out to the 
[PubChem Help mailing list](mailto:pubchem-help@ncbi.nlm.nih.gov)
for further support. 


## Acknowledgements

Many thanks to the PubChem team, especially Evan Bolton, Paul Thiessen 
and Jeff Zhang for their efforts related to the PubChem PFAS Tree
(also coauthors of the main document from which this content was extracted). Many thanks also to the ECI team for their efforts.

<!-- ## References {#refs} -->

