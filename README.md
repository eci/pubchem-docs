# PubChem Docs

A home for documentation, scripts etc related to PubChem efforts


| Topic | Documentation |
| ------ | ------ |
| Taxonomy | [_C. elegans_](https://git-r3lab.uni.lu/eci/pubchem-docs/-/tree/main/taxonomy/Celegans) as [PDF](https://git-r3lab.uni.lu/eci/pubchem-docs/-/raw/main/taxonomy/Celegans/C_elegans_metabolites.pdf?inline=false) or [RMarkdown](https://git-r3lab.uni.lu/eci/pubchem-docs/-/blob/main/taxonomy/Celegans/C_elegans_metabolites.Rmd) |
| PFAS Tree | [PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120) as [PDF](https://gitlab.lcsb.uni.lu/eci/pubchem-docs/-/raw/main/pfas-tree/PFAS_Tree.pdf?inline=false) or [RMarkdown](https://gitlab.lcsb.uni.lu/eci/pubchem-docs/-/blob/main/pfas-tree/PFAS_Tree.Rmd) |
| Merging Lists | Merging suspect lists in the [NORMAN-SLE Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=101) as [PDF](https://gitlab.lcsb.uni.lu/eci/pubchem-docs/-/raw/main/suspect-lists/Merging_Lists.pdf?inline=false) or [RMarkdown](https://gitlab.lcsb.uni.lu/eci/pubchem-docs/-/blob/main/suspect-lists/Merging_Lists.Rmd) |
| Ext. SMILES to SDF | Saving extended SMILES as SDF for deposition as [PDF](https://gitlab.lcsb.uni.lu/eci/pubchem-docs/-/raw/main/extSMILEStoSDF/extSMILEStoSDF.pdf?inline=false) or [RMarkdown](https://gitlab.lcsb.uni.lu/eci/pubchem-docs/-/raw/main/extSMILEStoSDF/extSMILEStoSDF.Rmd) |
| More soon... | More soon... |
